package com.getcha.auth.config

import com.getcha.auth.service.GetchaUserDetails
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices
import org.springframework.security.oauth2.provider.token.TokenStore
import javax.sql.DataSource


@Configuration
@EnableAuthorizationServer
class AuthorizationServerConfig(private val authenticationManager: AuthenticationManager,
                                private val dataSource: DataSource,
                                private val tokenStore: TokenStore,
                                private val bCryptPasswordEncoder: BCryptPasswordEncoder,
                                private val authorizationCodeServices: AuthorizationCodeServices,
                                private val userDetailsService: GetchaUserDetails) : AuthorizationServerConfigurerAdapter() {

    override fun configure(clients: ClientDetailsServiceConfigurer) {
        clients.jdbc(dataSource).passwordEncoder(bCryptPasswordEncoder)
                /*.withClient("foo")
                .secret("bar")
                .authorizedGrantTypes("password", "refresh", "implicit")
                .scopes("all", "write", "trust")
                .authorities("ROLE_USER", "ROLE_DEALER", "ROLE_ADMIN")
                .resourceIds("getcha")
                .accessTokenValiditySeconds(3600)
                .refreshTokenValiditySeconds(0)
                .and().build()*/
    }

    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {
        endpoints
                .authorizationCodeServices(authorizationCodeServices)
                .authenticationManager(authenticationManager)
                .tokenStore(tokenStore)
                .userDetailsService(userDetailsService)
    }

    override fun configure(security: AuthorizationServerSecurityConfigurer) {
        security.tokenKeyAccess("permitAll()")
                .checkTokenAccess("permitAll()")
    }
}