package com.getcha.auth.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.*
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2


@Configuration
@EnableSwagger2
@Profile("local")
class SwaggerConfiguration {

	val securitySchemaOAuth2 = "oauth2schema"
	val authorizationScopeGlobal = "global"
	val authorizationScopeGlobalDesc = "accessEverything"

	@Bean
	fun api(): Docket {
		return Docket(DocumentationType.SWAGGER_2)
				.groupName("api")
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.getcha.auth.controller"))
				.paths(PathSelectors.ant("/**"))
				.build()
				.securitySchemes(listOf(securitySchema()))
				.securityContexts(listOf(securityContext()))
	}

	private fun securitySchema(): OAuth {
		val authorizationScope = AuthorizationScope("all", "all desc")
		val loginEndpoint = LoginEndpoint("http://localhost:8080/oauth/authorize")
		val grantType = ImplicitGrant(loginEndpoint, "access_token")
		return OAuth(securitySchemaOAuth2, listOf(authorizationScope), listOf(grantType))
	}

	private fun securityContext(): SecurityContext {
		return SecurityContext.builder()
				.securityReferences(defaultAuth())
				.forPaths(PathSelectors.ant("/**"))
				.build()
	}

	private fun defaultAuth(): List<SecurityReference> {
		val authorizationScope = AuthorizationScope(authorizationScopeGlobal, authorizationScopeGlobalDesc)
		val authorizationScopes = arrayOfNulls<AuthorizationScope>(1)
		authorizationScopes[0] = authorizationScope
		return listOf(SecurityReference(securitySchemaOAuth2, authorizationScopes))
	}


	private fun apiInfo(): ApiInfo {
		return ApiInfoBuilder()
				.title("Getcha Auth Documentation")
				.description("Getcha Auth Client API Documentation")
				.build()
	}

}