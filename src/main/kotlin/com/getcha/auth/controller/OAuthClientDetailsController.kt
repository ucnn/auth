package com.getcha.auth.controller

import com.getcha.auth.model.OAuthClientDetails
import com.getcha.auth.service.OauthClientDetailsService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.security.access.prepost.PreAuthorize
//import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.validation.Valid
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Api("OauthClient")
@RestController
class OAuthClientDetailsController(private val oauthClientDetailsService: OauthClientDetailsService) {

    /*
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/oauth/clients")
    fun findClientSample(principal: Principal) : List<OAuthClientDetails> {
        val oauth = principal as OAuth2Authentication
        //TODO : oauth 조회
        return oauthClientDetailsService.findAll()
    }
    */

    @ApiOperation(value = "OAuthClient 조회", response = List::class)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/oauth/clients")
    fun findClients(principal: Principal) : List<OAuthClientDetails> = oauthClientDetailsService.findAll()

    @ApiOperation(value = "OAuthClient 추가", response = OAuthClientDetails::class)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/oauth/clients")
    fun addClient(@Valid @RequestBody oauthClientDetailsRequest: OAuthClientDetailsRequest)
            = oauthClientDetailsService.add(OAuthClientDetailsRequest.toModel(oauthClientDetailsRequest))

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/oauth/clients/{clientId}")
    fun updateClient(@PathVariable clientId: String, @Valid @RequestBody oauthClientDetailsRequest: OAuthClientDetailsRequest)
            = oauthClientDetailsService.modify(clientId = clientId, OAuthClientDetails = OAuthClientDetailsRequest.toModel(oauthClientDetailsRequest))

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/oauth/clients/{clientId}")
    fun deleteClient(@PathVariable clientId: String) = oauthClientDetailsService.delete(clientId)
}

data class OAuthClientDetailsRequest(
        @NotBlank
        val clientId: String = "",
        @NotBlank
        val resourceIds: String = "",
        @NotBlank
        val clientSecret: String = "",
        @NotBlank
        val scope: String = "",
        @NotBlank
        val authorizedGrantTypes: String = "",
        @NotNull
        val webServerRedirectUri: String = "",
        @NotBlank
        val authorities: String = "",
        @NotNull
        val accessTokenValidity: Long,
        @NotNull
        val refreshTokenValidity: Long,
        val additionalInformation: String = "{}",
        @NotNull
        val autoapprove: String = ""
) {
    companion object {
        fun toModel(OAuthClientDetailsRequest: OAuthClientDetailsRequest): OAuthClientDetails {
            return OAuthClientDetails(clientId = OAuthClientDetailsRequest.clientId,
                    resourceIds = OAuthClientDetailsRequest.resourceIds,
                    clientSecret = OAuthClientDetailsRequest.clientSecret,
                    scope = OAuthClientDetailsRequest.scope,
                    authorizedGrantTypes = OAuthClientDetailsRequest.authorizedGrantTypes,
                    webServerRedirectUri = OAuthClientDetailsRequest.webServerRedirectUri,
                    authorities = OAuthClientDetailsRequest.authorities,
                    accessTokenValidity = OAuthClientDetailsRequest.accessTokenValidity,
                    refreshTokenValidity = OAuthClientDetailsRequest.refreshTokenValidity,
                    additionalInformation = OAuthClientDetailsRequest.additionalInformation,
                    autoapprove = OAuthClientDetailsRequest.authorities)
        }
    }
}