package com.getcha.auth.controller

import com.getcha.auth.model.User
import com.getcha.auth.service.UserService
import org.springframework.web.bind.annotation.*

@RestController
class UserController(private val userService: UserService) {

    @PostMapping("/users/registration")
    fun reg(@RequestBody user: UserRequest) = userService.registration(UserRequest.toModel(user))

    @GetMapping("/users/{id}")
    fun findUser(@PathVariable id: Long) = userService.findByIdWithAuth(id)
}

data class UserRequest(val username: String,
                       val password: String) {
    companion object {
        fun toModel(userRequest: UserRequest) = User(
                username = userRequest.username,
                password = userRequest.password,
                role = "USER")
    }
}