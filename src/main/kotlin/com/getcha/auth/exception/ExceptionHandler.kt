package com.getcha.auth.exception

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.core.env.Environment
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestControllerAdvice
class RestExceptionConfiguration(private val env: Environment) : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [GetchaException::class, RuntimeException::class])
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    fun handleDevoteException(e: RuntimeException, request: WebRequest): Map<String, Any> =
            errorAttributes(HttpStatus.INTERNAL_SERVER_ERROR, request)

    @ExceptionHandler(value = [NotFoundException::class])
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    fun handleNotFoundException(e: RuntimeException, request: WebRequest): Map<String, Any> =
            errorAttributes(HttpStatus.NOT_FOUND, request)

    @ExceptionHandler(value = [BadRequestException::class])
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    fun handleBadRequestException(e: RuntimeException, request: WebRequest): Map<String, Any> =
            errorAttributes(HttpStatus.BAD_REQUEST, request)


    private fun isIncludeStackTrace(): Boolean {
        val profiles = env.activeProfiles
        return (profiles.isEmpty() || profiles.contains("local") || profiles.contains("dev"))
    }

    private fun errorAttributes(status: HttpStatus, request: WebRequest) =
            DefaultErrorAttributes().getErrorAttributes(request, isIncludeStackTrace()).apply {
                put("status", status.value())
                put("error", status)
            }
}