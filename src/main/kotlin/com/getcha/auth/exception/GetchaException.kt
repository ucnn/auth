package com.getcha.auth.exception

import org.springframework.validation.BindingResult

open class GetchaException(message: String) : RuntimeException(message)

class NotFoundException(message: String) : GetchaException(message)

open class BadRequestException(message: String) : GetchaException(message)

class InvalidParameterException(message: String, val errors: BindingResult) : BadRequestException(message)