package com.getcha.auth.model

import com.getcha.auth.repository.Audit
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "User")
data class User(
        var username: String,

        var password: String,

        var role: String
) : Audit()

@Entity
data class OAuthClientDetails(

        @Id
        @Column(updatable = false, nullable = false)
        var clientId: String,

        var resourceIds: String,

        var clientSecret: String,

        var scope: String,

        var authorizedGrantTypes: String,

        var webServerRedirectUri: String,

        var authorities: String,

        var accessTokenValidity: Long,

        var refreshTokenValidity: Long,

        var additionalInformation: String,

        var autoapprove: String
)