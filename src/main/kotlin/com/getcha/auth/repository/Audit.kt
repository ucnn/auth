package com.getcha.auth.repository

import org.hibernate.annotations.GenericGenerator
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import javax.persistence.*

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class Audit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(updatable = false, nullable = false)
    var id: Long? = null

    @Column(nullable = false)
    @CreatedDate
    lateinit var createdAt: LocalDateTime

    @Column(nullable = false)
    @LastModifiedDate
    lateinit var updatedAt: LocalDateTime
}