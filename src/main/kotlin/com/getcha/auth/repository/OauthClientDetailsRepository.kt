package com.getcha.auth.repository

import com.getcha.auth.model.OAuthClientDetails
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface OauthClientDetailsRepository : JpaRepository<OAuthClientDetails, String>