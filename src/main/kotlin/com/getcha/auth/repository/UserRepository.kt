package com.getcha.auth.repository

import com.getcha.auth.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : JpaRepository<User, Long> {
    fun findOneByUsername(username: String) : Optional<User>
}