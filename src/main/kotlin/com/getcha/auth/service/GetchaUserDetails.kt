package com.getcha.auth.service

import com.getcha.auth.repository.UserRepository
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class GetchaUserDetails(private val userRepository: UserRepository) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findOneByUsername(username)
                .orElseThrow { UsernameNotFoundException("not found user") }

        return User.builder()
                .username(user.username)
                .password(user.password)
                .roles(getRoles(user.role))
                .build()
    }

    private fun getRoles(role: String) : String {
        return when (role) {
            "USER" -> "USER"
            "DEALER" -> "DEALER"
            "ADMIN" -> "ADMIN"
            else -> "NONE"
        }
    }

}