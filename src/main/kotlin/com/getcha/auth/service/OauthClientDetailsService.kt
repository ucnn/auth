package com.getcha.auth.service

import com.getcha.auth.exception.NotFoundException
import com.getcha.auth.model.OAuthClientDetails
import com.getcha.auth.repository.OauthClientDetailsRepository
import org.springframework.stereotype.Service

@Service
class OauthClientDetailsService(private val oauthClientDetailsRepository: OauthClientDetailsRepository) {

    fun findAll(): MutableList<OAuthClientDetails> = oauthClientDetailsRepository.findAll()

    fun add(OAuthClientDetails: OAuthClientDetails) : OAuthClientDetails = oauthClientDetailsRepository.saveAndFlush(OAuthClientDetails)

    fun modify(clientId: String, OAuthClientDetails: OAuthClientDetails) : OAuthClientDetails {
        val client = oauthClientDetailsRepository.findById(clientId).orElseThrow { NotFoundException("Not found client : $clientId") }

        client.apply {
            resourceIds = OAuthClientDetails.resourceIds
            scope = OAuthClientDetails.scope
            authorizedGrantTypes = OAuthClientDetails.authorizedGrantTypes
            webServerRedirectUri = OAuthClientDetails.webServerRedirectUri
            authorities = OAuthClientDetails.authorities
            accessTokenValidity = OAuthClientDetails.accessTokenValidity
            refreshTokenValidity = OAuthClientDetails.refreshTokenValidity
            additionalInformation = OAuthClientDetails.additionalInformation
            autoapprove = OAuthClientDetails.autoapprove
        }

        return oauthClientDetailsRepository.saveAndFlush(client)
    }

    fun delete(clientId: String) = oauthClientDetailsRepository.deleteById(clientId)
}