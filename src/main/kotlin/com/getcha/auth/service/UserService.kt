package com.getcha.auth.service

import com.getcha.auth.exception.BadRequestException
import com.getcha.auth.exception.NotFoundException
import com.getcha.auth.model.User
import com.getcha.auth.repository.UserRepository
import org.springframework.security.access.prepost.PostAuthorize
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserService(
        private val bCryptPasswordEncoder: BCryptPasswordEncoder,
        private val userRepository: UserRepository) {

    @Transactional
    fun registration(user: User) : User {
        userRepository.findOneByUsername(user.username).ifPresent { throw BadRequestException("Already registration user") }
        user.password = bCryptPasswordEncoder.encode(user.password)
        return userRepository.save(user)
    }

    @Transactional(readOnly = true)
    @PostAuthorize("isAuthenticated() and returnObject.username == authentication.name OR hasRole('ROLE_ADMIN')")
    fun findByIdWithAuth(id: Long) = userRepository.findById(id).orElseThrow { NotFoundException("not found user: id = $id") }!!
}