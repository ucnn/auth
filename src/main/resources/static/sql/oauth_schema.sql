create table if not exists oauth_access_token
(
  token_id varchar(256) null primary key,
  token mediumblob null,
  authentication_id varchar(256) null,
  user_name varchar(256) null,
  client_id varchar(256) null,
  authentication mediumblob null,
  refresh_token varchar(256) null
);

create table if not exists oauth_client_details
(
  client_id varchar(256) not null primary key,
  resource_ids varchar(256) null,
  client_secret varchar(256) null,
  scope varchar(256) null,
  authorized_grant_types varchar(256) null,
  web_server_redirect_uri varchar(256) null,
  authorities varchar(256) null,
  access_token_validity int null,
  refresh_token_validity int null,
  additional_information varchar(4096) null,
  autoapprove varchar(256) null
);

create table if not exists oauth_client_token
(
  token_id varchar(256) null primary key,
  token mediumblob null,
  authentication_id varchar(256) null,
  user_name varchar(256) null,
  client_id varchar(256) null
);

create table if not exists oauth_code
(
  code varchar(256) null,
  authentication mediumblob null
);

create table if not exists oauth_refresh_token
(
  token_id varchar(256) null primary key,
  token mediumblob null,
  authentication mediumblob null
);

